package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	EmailValidator method = new EmailValidator();
	
	
	//format check
	@Test
	public void testCorrectFormatNormal() {
		assertTrue(method.hasCorrectFormat("tuan2000@gmail.com"));
	}
	
	@Test
	public void testCorrectFormatException() {
		assertTrue(method.hasCorrectFormat("tuan2000@gmail.com"));
	}

	@Test
	public void testCorrectFormatBoundaryIn() {
		assertTrue(method.hasCorrectFormat("t@gmail.com"));
	}

	@Test
	public void testCorrectFormatBoundaryOut() {
		assertTrue(method.hasCorrectFormat("@gmail.com"));
	}

	//@ character check
	@Test
	public void testHasNumberOfAtCharNormal() {
		assertTrue(method.hasOneAtChar("tuan2000@gmail.com"));
	}
	
	@Test
	public void testHasNumberOfAtCharException() {
		assertFalse(method.hasOneAtChar("tuan2000@@@@gmail.com"));
	}
	
	@Test
	public void testHasNumberOfAtCharBoundaryIn() {
		assertTrue(method.hasOneAtChar("tuan2000@gmail.com"));
	}
	
	@Test
	public void testHasNumberOfAtCharBoundaryOut() {
		assertFalse(method.hasOneAtChar("tuan2000@@gmail.com"));
	}
	
	//account name format check
	@Test
	public void testhasValidAccountNameNormal() {
		assertTrue(method.hasValidAccount("tuan2000@gmail.com"));
	}
	
	@Test
	public void testhasValidAccountNameException() {
		assertFalse(method.hasValidAccount("2tuan2000@gmail.com"));
	}
	@Test
	public void testhasValidAccountNameBoundaryIn() {
		assertTrue(method.hasValidAccount("tua@gmail.com"));
	}
	@Test
	public void testhasValidAccountNameBoundaryOut() {
		assertFalse(method.hasValidAccount("tu@gmail.com"));
	}
	
	//domain name check
	@Test
	public void testhasValidDomainNameNormal() {
		assertTrue(method.hasValidDomain("tuan2000@gmail.com"));
	}
	
	@Test
	public void testhasValidDomainNameBoundaryIn() {
		assertTrue(method.hasValidDomain("tuan2000@g5.com"));
	}
	
	@Test
	public void testhasValidDomainNameBoundaryOut() {
		assertFalse(method.hasValidDomain("tuan2000@g.com"));
	}
	
	//extension name check
	@Test
	public void testhasValidExtensionNameNormal() {
		assertTrue(method.hasValidExtension("tuan2000@gmail.com"));
	}
	
	@Test
	public void testhasValidExtensionNameBoundaryIn() {
		assertTrue(method.hasValidExtension("tuan2000@gmail.ca"));
	}
	
	@Test
	public void testhasValidExtensionNameBoundaryOut() {
		assertFalse(method.hasValidExtension("tuan2000@gmail.f"));
	}
	
	//intergration of all check test
	@Test
	public void testhasValidEmailNormal() {
		assertTrue(method.isValidEmail("tuan2000@gmail.com"));
	}
	
	@Test
	public void testhasValidEmailOut() {
		assertFalse(method.isValidEmail("2tuan2000@g.f"));
	}
	
	
}
