package sheridan;

public class EmailValidator {
	
	private String EMAIL_REGEX = ".*@.*\\..*";
	private int NUMBER_OF_ACCOUNT_LOWERCASE = 3;
	private int NUMBER_OF_DOMAIN_LOWERCASE = 2;
	private int NUMBER_OF_EXTENSION_LOWERCASE = 2;
	
	public boolean isValidEmail(String input) {
		return hasCorrectFormat(input) & hasOneAtChar(input) & hasValidAccount(input) & hasValidDomain(input) & hasValidExtension(input) ;
	}
	
	public boolean hasCorrectFormat(String input) {
		return input.matches(EMAIL_REGEX);
	}
	
	public boolean hasOneAtChar(String input) {
		int count = 0;
		for(int i =0;i<input.length();i++) {	
			if(input.charAt(i) == '@') {
				count++;
			}
		}
		return (count == 1)? true : false;
	}
	
	public boolean hasValidAccount(String input) {
		input = input.substring(0, input.indexOf('@'));
		if(Character.isDigit(input.charAt(0))) {
			return false;
		}
		int count = 0;
		for(int i =0;i<input.length();i++) {	
			if(Character.isLowerCase(input.charAt(i))) {
				count++;
			}
		}
		return (count >= NUMBER_OF_ACCOUNT_LOWERCASE);
	}
	
	public boolean hasValidDomain(String input) {
		input = input.substring(input.indexOf('@')+1, input.indexOf('.'));
		int count = 0;
		for(int i =0;i<input.length();i++) {	
			if(Character.isLowerCase(input.charAt(i)) || Character.isDigit(input.charAt(i))) {
				count++;
			}
		}
		return (count >= NUMBER_OF_DOMAIN_LOWERCASE);
	}
	
	public boolean hasValidExtension(String input) {
		input = input.substring(input.lastIndexOf('.')+1);
		return (input.length() >= NUMBER_OF_EXTENSION_LOWERCASE);
	}
}
